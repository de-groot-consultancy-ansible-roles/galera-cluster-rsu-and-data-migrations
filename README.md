# Galera Cluster safe RSU

This role will safely perform RSU on a Galera cluster. At the moment only the RSU works, there is no confirmation if the schema change execute is backwards compatible (yet).

Example playbook:
```
- hosts:
    - db-cluster
  become: true
  serial: 1
  vars:
    rsu_statement: "TRUNCATE TABLE dba.hello;"
    mysql_login_user: "root"
    mysql_login_host: "localhost"
    mysql_login_password: ""
    # Credentials arranged through /root/.my.cnf.
  roles:
    - galera-cluster-safe-rsu
```
## Asynchronous replica's
Next to Galera nodes, you can alter asynchronous replica's. This is useful if those replica's are set up to connect to via ProxySQL to a Galera cluster. In this case it is recommended to ignore the GTID domain ids that are configured for the individual cluster members, but not for the cluster itself. To clarify: `wsrep_gtid_domain_id` should NOT be ignored, while each cluster member's different `gtid_domain_id` SHOULD be ignored. This will result in the replica being modified, and still be able to reconnect to a different cluster member.
The ALTER statement on the replica's is executed with STOP ALL SLAVES, configurable SLEEP(5) and START ALL SLAVES around. Errors on the replica are ignored so that (for example) adding foreign keys on storage engines that do not support foreign keys is supported (in case the replica has a different storage engine). In case the replica is a Galera cluster, the ALTER statement is executed TOI, causing an outage on that replica galera cluster. Pull requests to fix this are welcome.
You can specify a group of hosts or specific replica's to ALTER:
- `rsu_replica_group`: "replica"
- `rsu_replica_hosts`: "{{ groups[rsu_replica_group] if rsu_replica_group else [] }}"

# Combination with DBA toolkit
This RSU system can be combined with the procedures in the [DBA Toolkit](https://gitlab.com/de-groot-consultancy-ansible-roles/dba-toolkit/). For example, to mass optimize all tables in a schema you can execute this playbook:
```
- hosts:
    - db-cluster
  become: true
  serial: 1
  vars:
    dba_toolkit_install_procedures_schema: "dba"
    rsu_statement: "CALL mass_optimize_tables('dba', 'app1', '%\_log')"
    mysql_login_user: "root"
    mysql_login_host: "localhost"
    mysql_login_password: ""
    # Credentials arranged through /root/.my.cnf.
  roles:
    - dba-toolkit
    - galera-cluster-safe-rsu
```
This will optimize all tables that end with _log in the app1 schema.

TODO:
- Verify schema change is backwards compatbile
- Verify all cluster nodes are being changed with this RSU

License: AGPL

Author: Michaël de Groot
